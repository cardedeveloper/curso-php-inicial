//variable global para los cursos
var cursos = [];
//incializar el arreglo;
cargar();

function findcurso (cursoId) {
  //console.log("findCurso");
  return cursos[findcursoKey(cursoId)];
};

function findcursoKey (cursoId) {
  //console.log("findCursoKey");
  for (var key = 0; key < cursos.length; key++) {
    if (cursos[key].id == cursoId) {
      return key;
    }
  }
};

var List = Vue.extend({
  template: '#curso-list',
  data: function () {
    return {cursos: cursos, searchKey: ''};
  },
  computed: {
    filteredcursos: function () {
      return this.cursos.filter(function (curso) {
       // console.log(this.searchKey.toUpperCase());
        return this.searchKey=='' || curso.name.toUpperCase().indexOf(this.searchKey.toUpperCase()) !== -1;
      },this);
    }
  }
});

var curso = Vue.extend({
  template: '#curso',
  data: function () {
    return {curso: findcurso(this.$route.params.curso_id)};
  }
});

var cursoEdit = Vue.extend({
  template: '#curso-edit',
  data: function () {
    return {curso: findcurso(this.$route.params.curso_id)};
  },
  methods: {
    updatecurso: function () {
      var curso = this.curso;
      actualizar({id: curso.id,name: curso.name,description: curso.description,price: curso.price});
      cursos[findcursoKey(curso.id)] = {
        id: curso.id,
        name: curso.name,
        description: curso.description,
        price: curso.price
      };
      router.push('/');
    }
  }
});

var cursoDelete = Vue.extend({
  template: '#curso-delete',
  data: function () {
    return {curso: findcurso(this.$route.params.curso_id)};
  },
  methods: {
    deletecurso: function () {
      eliminar(this.$route.params.curso_id);
      cursos.splice(findcursoKey(this.$route.params.curso_id), 1);
      router.push('/');
    }
  }
});

var Addcurso = Vue.extend({
  template: '#add-curso',
  data: function () {
    return {curso: {name: '', description: '', price: ''}}
  },
  methods: {
    createcurso: function() {
      var curso = this.curso;
      var nuevo= agregar({name: curso.name,description: curso.description,price: curso.price});
      cursos.push({
        id: nuevo,
        name: curso.name,
        description: curso.description,
        price: curso.price
      });
      router.push('/');
    }
  }
});
/***********   Rutas VueJs   *********/
var router = new VueRouter({routes:[
  { path: '/', component: List},
  { path: '/curso/:curso_id', component: curso, name: 'curso'},
  { path: '/add-curso', component: Addcurso},
  { path: '/curso/:curso_id/edit', component: cursoEdit, name: 'curso-edit'},
  { path: '/curso/:curso_id/delete', component: cursoDelete, name: 'curso-delete'}
]});
//Inicializar vueJs
app = new Vue({
  router:router
}).$mount('#app')


/*******   Funciones AJAX *******/

//incializa la variable de los cursos
function cargar(){
  console.log("cargo");
cursos.push({id: 1, name: 'Angular', description: 'Superheroic JavaScript MVW Framework.', price: 100});
cursos.push({id: 2, name: 'Ember', description: 'A framework for creating ambitious web applications.', price: 150});
cursos.push({id: 3, name: 'React', description: 'A JavaScript Library for building user interfaces.', price: 200});
  
}

//Agrega cursos

function agregar(curso){
  console.log("agrego");
  return Math.random().toString().split('.')[1];
}

//Eliminar curso

function eliminar(id){
  console.log("elimino");
}

// Actualizar un curso

function actualizar(curso){
  console.log("actualizo");
}